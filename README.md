# LTemplate

This is a package that simplifies extending _Mathematica_ through LibraryLink by automating the generation of boilerplate code.

At present, the package is still in flux and not ready for general use.  However, all constructive criticism is welcome.

To install, drop the `LTemplate` directory in the directory opened by `SystemOpen@FileNameJoin[{$UserBaseDirectory, "Applications"}]`.

See `Documentation/LTemplateTutorial.nb` for a short introduction through examples.